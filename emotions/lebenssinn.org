#+title: Lebenssinn

#+begin_quote
Ist die Schnittmenge von Spaß, Talent und Nutzen.
#+end_quote
* Was tue ich am liebsten?
* Entspricht es auch meinen Talenten?
* Inwieweit hilft es anderen?
