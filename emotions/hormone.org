#+title: Hormone

| / | <>        | <>         | <>                     | <>                                                                                       |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
|   | Hormone   | Purpose    | Increase               | Further information                                                                      |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
|   | Dopamin   | Pleasure   | Foods high in tyrosine | [[https://www.health.harvard.edu/mind-and-mood/dopamine-the-pathway-to-pleasure]]            |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
|   | Serotonin | Mood       | Work out               | [[https://www.health.harvard.edu/mind-and-mood/serotonin-the-natural-mood-booster]]          |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
|   | Endorphin | Painkiller | Aerobic excercise      | [[https://www.health.harvard.edu/mind-and-mood/endorphins-the-brains-natural-pain-reliever]] |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
|   | Oxytocin  | Love       | Touch                  | [[https://www.health.harvard.edu/mind-and-mood/oxytocin-the-love-hormone]]                   |
|---+-----------+------------+------------------------+------------------------------------------------------------------------------------------|
