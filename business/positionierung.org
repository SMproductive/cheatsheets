#+title: Positionierung

* Grundsätze
** Anders, nicht besser
Was bekommt der Kunde von dir, was er von keinem anderen bekommt?
** Außergewöhnlich, nicht excellent
Gute Leistung ist kein Ersatz für das Außergewöhnliche. Nur das Außergewöhnliche prägt sich ein. Nur wer die Dinge außergewöhnlich macht, wird groß.
** Der Erste sein
Je kleiner die Nische, desto leichter ist es, Erster zu sein.
** Eine neue Kategorie erfinden
Die Zukunft erfinden ... Repariere, was nicht kaput ist.
** Besser spitz als breit
Je größer das Angebot, desto schwerer ist es, Aufmerksamkeit zu erringen und desto weniger glaubwürdig bist du. Spitz dringt besser in den Markt ein - das ist ein Naturgesetz.
** Ein Grundbedürfniss wählen, kein besonderes Verfahren
Wer sich auf Grundbedürfnisse spezialisiert, vertritt die Interessen der Kunden, wirkt unabhängig. Wer sich auf ein Verfahren verlässt, lebt gefährlich und ist nicht objektiv.
** Kleine Zielgruppe
Bilde dein Geschäft nicht um die Kunden herum, die du bereits hast, sondern bilde dein Geschäft in einer Weise, dass du die Kunden anziehst, die du gerne haben möchtest.
** Löse für andere ein Problem
Je größer das Problem, desto stärker der Wunsch des Kunden, einen wirklichen Spezialisten zu finden. Überlege, welches Bedürfnis du befriedigst, wenn du das Problem löst.
** Rede darüber
Du musst auf dich aufmerksam machen. Positionierung bedeutet nicht nur seine genaue Position zu kennen, sondern auch, sie anderen mitzuteilen - deutlich und klar.
** Bestimme den Preis
Experten können den Preis bestimmen; wer nicht anders ist, muss einen gnadenlosen Preiswettbewerb mitmachen.
* Positionierungssatz
1. Was liebst du? Was ist dein warum?
2. Wofür bist du Experte?
3. Welchen Nutzen hat die Zielgruppe?
* Bedürfnisse
Die linken und rechten Bedürfnisse sind Paradoxa. Wird durch ein Produkt ein Bedürfnis erfüllt, so entsteht dann ein größeres Bedürfnis nach dem Gegensatz. Das heißt, das Nachfolgeprodukt sollte dann das neue Bedürfnis miteinbeziehen.
| / | <>                        | <>                        |
|---+---------------------------+---------------------------|
|   | *Bedürfnis L*             | *Bedürfnis R*             |
|---+---------------------------+---------------------------|
|---+---------------------------+---------------------------|
|   | Freiheit/Abenteuer        | Sicherheit/Bequemlichkeit |
|---+---------------------------+---------------------------|
|   | Einzigartigkeit/Bedeutung | Liebe/Verbindung          |
|---+---------------------------+---------------------------|
|   | Wachstum                  | Geben                     |
|---+---------------------------+---------------------------|
* Marktforschung
Der Kunde ist dein bester Unternehmensberater. Was ist der Nutzen des Nutzen?
* Werbung
- Versprechen mit Zeitangabe
